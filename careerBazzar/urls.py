import os
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^captcha/', include('captcha.urls')),
    
    # Homes URL
    url(r'^careerbazzar/home/', 'homes.views.homePage', name='home'),
    url(r'^careerbazzar/new/job/', 'homes.views.jobPost', name='job_post'),
    url(r'^careerbazzar/edit_job/(?P<job_id>\d+)/', 'homes.views.jobPostEdit', name='job_edit'),
    url(r'^careerbazzar/browse-jobs/', 'homes.views.browseJobs', name='browse_job'),
    url(r'^careerbazzar/job/(?P<job_id>\d+)/', 'homes.views.viewJob', name='job'),
    url(r'^careerbazzar/apply/(?P<job_id>\d+)/', 'homes.views.applyJob', name='apply'),
    url(r'^careerbazzar/category/(?P<category>\S+)/', 'homes.views.filterCategory', name='category'),
    url(r'^careerbazzar/search/results/', 'homes.views.searchResult1', name='search1'),
    url(r'^careerbazzar/search-results/', 'homes.views.searchResult2', name='search2'),

    # Forums URL
    url(r'^careerbazzar/forum/home/', 'forum.views.blogHome', name='blog_home'),
    url(r'^careerbazzar/forum/tag/(?P<category>\S+)/', 'forum.views.category_post', name='tag'),
    url(r'^careerbazzar/forum/new_post/', 'forum.views.new_post', name='new_article'),
    url(r'^careerbazzar/forum/search_result/page/(?P<page>\d+)/', 'forum.views.search_result', name='search_result'),
    url(r'^careerbazzar/forum/article/(?P<post_id>\d+)/', 'forum.views.view_post', name='article'),
    url(r'^careerbazzar/forum/article/edit/(?P<post_id>\d+)/', 'forum.views.edit_post', name='edit_article'),
    url(r'^careerbazzar/forum/delete_post/(?P<post_id>\d+)/', 'forum.views.delete_post', name='delete_article'),
    url(r'^careerbazzar/forum/pin_post/(?P<post_id>\d+)/', 'forum.views.pin_post', name='pin_post'),
    url(r'^careerbazzar/forum/unpin_post/(?P<post_id>\d+)/', 'forum.views.unpin_post', name='unpin_post'),

    # Authorization URL
    url(r'^careerbazzar/login/', 'authorize.views.login', name='login'),
    url(r'^careerbazzar/logout/', 'authorize.views.logout', name='logout'),
    url(r'^careerbazzar/invalid/', 'authorize.views.invalid_login', name='invalid'),
    url(r'^careerbazzar/signup/', 'authorize.views.user_signup', name='signup'),
    url(r'^careerbazzar/employer/signup/', 'authorize.views.employer_signup', name='employer_signup'),

    # Profiles URL
    url(r'^careerbazzar/profile/(?P<username>\S+)/', 'profiles.views.profile', name='profile'),
    url(r'^careerbazzar/user/(?P<username>\S+)/', 'profiles.views.user_profile', name='uprofile'),
    url(r'^careerbazzar/edit_user/(?P<username>\S+)/', 'profiles.views.edit_uprofile', name='edit_uprofile'),
    url(r'^careerbazzar/employer/(?P<username>\S+)/', 'profiles.views.employer_profile', name='eprofile'),
    url(r'^careerbazzar/edit/employer/(?P<username>\S+)/', 'profiles.views.edit_eprofile', name='edit_eprofile'),
    url(r'^careerbazzar/view/applicant/(?P<username>\S+)/(?P<job_id>\d+)/', 'profiles.views.view_applicants', name='view_app'),
    url(r'^careerbazzar/change/password/', 'profiles.views.change_password', name='chpass'),
    url(r'^careerbazzar/delete/account/', 'profiles.views.delete_account', name='daccount'),
    url(r'^careerbazzar/about/', 'profiles.views.about', name='about'),
    url(r'^careerbazzar/terms/', 'profiles.views.terms', name='terms'),
    url(r'^careerbazzar/privacy-policy/', 'profiles.views.privacy', name='privacy'),
    url(r'^careerbazzar/faq/', 'profiles.views.faq', name='faq'),

    #SiteAdmin ---------------------------------------------------------------------------

    url(r'^careerbazzar/siteadmin/admin_panel/',    'siteAdmin.views.admin_panel',name="admin_panel"),
    url(r'^careerbazzar/siteadmin/add_job_category/', 'siteAdmin.views.add_job_category',name='add_job_category'),
    url(r'^careerbazzar/siteadmin/edit_job_category/(?P<cat_id>\d+)/', 'siteAdmin.views.edit_job_category',name="edit_job_cat"),
    url(r'^careerbazzar/siteadmin/del_job_cat/(?P<cat_id>\d+)/', 'siteAdmin.views.del_job_cat',name="del_job_cat"),
    url(r'^careerbazzar/siteadmin/add_job_type/',  'siteAdmin.views.add_job_type',name="add_job_type"),
    url(r'^careerbazzar/siteadmin/edit_job_type/(?P<type_id>\d+)/',  'siteAdmin.views.edit_job_type',name="edit_job_type"),
    url(r'^careerbazzar/siteadmin/del_job_type/(?P<type_id>\d+)/',  'siteAdmin.views.del_job_type',name="del_job_type"),
    url(r'^careerbazzar/siteadmin/add_job_location/',  'siteAdmin.views.add_job_location',name="add_job_location"),
    url(r'^careerbazzar/siteadmin/edit_job_location/(?P<location_id>\d+)/',  'siteAdmin.views.edit_job_location',name="edit_job_location"),
    url(r'^careerbazzar/siteadmin/del_job_location/(?P<location_id>\d+)/',  'siteAdmin.views.del_job_location',name="del_job_location"),
    url(r'^careerbazzar/siteadmin/add_job_role/',  'siteAdmin.views.add_job_role',name="add_job_role"),
    url(r'^careerbazzar/siteadmin/edit_job_role/(?P<role_id>\d+)/',  'siteAdmin.views.edit_job_role',name="edit_job_role"),
    url(r'^careerbazzar/siteadmin/del_job_role/(?P<role_id>\d+)/',  'siteAdmin.views.del_job_role',name="del_job_role"),
    url(r'^careerbazzar/siteadmin/add_article_category/', 'siteAdmin.views.add_article_category',name="add_article_category"),
    url(r'^careerbazzar/siteadmin/edit_art_category/(?P<cat_id>\d+)/', 'siteAdmin.views.edit_art_category',name="edit_art_category"),
    url(r'^careerbazzar/siteadmin/del_art_category/(?P<cat_id>\d+)/', 'siteAdmin.views.del_art_category',name="del_art_category"),
    url(r'^careerbazzar/siteadmin/add_site_about/', 'siteAdmin.views.add_site_about',name="add_site_about"),
    url(r'^careerbazzar/siteadmin/edit_site_about/(?P<cat_id>\d+)/', 'siteAdmin.views.edit_site_about',name="edit_site_about"),
    url(r'^careerbazzar/siteadmin/del_site_about/(?P<cat_id>\d+)/', 'siteAdmin.views.del_site_about',name="del_site_about"),
    url(r'^careerbazzar/siteadmin/add_site_term/','siteAdmin.views.add_site_term',name="add_site_term"),
    url(r'^careerbazzar/siteadmin/edit_site_term/(?P<term_id>\d+)/', 'siteAdmin.views.edit_site_term',name="edit_site_term"),
    url(r'^careerbazzar/siteadmin/del_site_term/(?P<term_id>\d+)/', 'siteAdmin.views.del_site_term',name="del_site_term"),
    url(r'^careerbazzar/siteadmin/add_privacy_policy/', 'siteAdmin.views.add_privacy_policy',name="add_privacy_policy"),
    url(r'^careerbazzar/siteadmin/edit_privacy_policy/(?P<policy_id>\d+)/', 'siteAdmin.views.edit_privacy_policy',name="edit_privacy_policy"),
    url(r'^careerbazzar/siteadmin/del_privacy_policy/(?P<policy_id>\d+)/','siteAdmin.views.del_privacy_policy',name="del_privacy_policy"),
    url(r'^careerbazzar/siteadmin/add_faq/', 'siteAdmin.views.add_faq',name="add_faq"),
    url(r'^careerbazzar/siteadmin/edit_faq/(?P<faq_id>\d+)/', 'siteAdmin.views.edit_faq',name="edit_faq"),
    url(r'^careerbazzar/siteadmin/del_faq/(?P<faq_id>\d+)/',  'siteAdmin.views.del_faq',name="del_faq"),
    url(r'^careerbazzar/siteadmin/set_featured_jobs/',  'siteAdmin.views.set_featured_job',name="set_featured_job"),
    url(r'^careerbazzar/siteadmin/make_featured/(?P<job_id>\d+)/',  'siteAdmin.views.make_featured',name="make_featured"),
    
    url(r'^careerbazzar/siteadmin/add_salary_range/','siteAdmin.views.add_salary_range', name="add_salary_range"),
    url(r'^careerbazzar/siteadmin/edit_salary_range/(?P<salary_id>\d+)/',  'siteAdmin.views.edit_salary_range',name="edit_salary_range"),
    url(r'^careerbazzar/siteadmin/del_salary_range/(?P<salary_id>\d+)/','siteAdmin.views.del_salary_range',name='del_salary_range'),

    url(r'^careerbazzar/siteadmin/add_ads_left/','siteAdmin.views.add_ads_left',name='add_ads_left'),
    url(r'^careerbazzar/siteadmin/edit_ads_left/(?P<ads_id>\d+)/','siteAdmin.views.edit_ads_left',name='edit_ads_left'),
    url(r'^careerbazzar/siteadmin/del_ads_left/(?P<ads_id>\d+)/','siteAdmin.views.del_ads_left',name='del_ads_left'),

    url(r'^careerbazzar/siteadmin/add_ads_right/','siteAdmin.views.add_ads_right',name='add_ads_right'),
    url(r'^careerbazzar/siteadmin/edit_ads_right/(?P<ads_id>\d+)/','siteAdmin.views.edit_ads_right',name='edit_ads_right'),
    url(r'^careerbazzar/siteadmin/del_ads_right/(?P<ads_id>\d+)/','siteAdmin.views.del_ads_right',name='del_ads_right'),

    url(r'^careerbazzar/siteadmin/add_ads_bottom/','siteAdmin.views.add_ads_bottom',name='add_ads_bottom'),
    url(r'^careerbazzar/siteadmin/edit_ads_bottom/(?P<ads_id>\d+)/','siteAdmin.views.edit_ads_bottom',name='edit_ads_bottom'),
    url(r'^careerbazzar/siteadmin/del_ads_bottom/(?P<ads_id>\d+)/','siteAdmin.views.del_ads_bottom',name='del_ads_bottom'),

    
    url(r'^careerbazzar/siteadmin/notifications/', 'siteAdmin.views.notifications',name="notifications"),
    url(r'^careerbazzar/siteadmin/admin_view_job/(?P<job_id>\d+)/',  'siteAdmin.views.admin_view_job',name="admin_view_job"),
    url(r'^careerbazzar/siteadmin/job_approval/(?P<job_id>\d+)/',  'siteAdmin.views.job_approval',name="job_approval"),
    url(r'^careerbazzar/siteadmin/del_job_approval/(?P<job_id>\d+)/',  'siteAdmin.views.del_job_approval',name="del_job_approval"),
    url(r'^careerbazzar/siteadmin/ajax_notifications/',  'siteAdmin.views.ajax_notifications',name="ajax_notifications"),

    #SiteAdmin ---------------------------------------------------------------------------

    # Redirect URL
    url(r'^careerbazzar.com/', 'homes.views.go_to_Home'),
    url(r'^$', 'homes.views.go_to_Site'),
)
'''
urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)
'''

urlpatterns += static(settings.STATIC_URL,
    document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)
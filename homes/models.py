from django.db import models

class allJob(models.Model):
	companyName = models.CharField(max_length=160)
	companyEmail = models.EmailField(max_length=254)
	jobTitle = models.TextField()
	jobCategory = models.CharField(max_length=60)
	jobType = models.CharField(max_length=60)
	jobSalary = models.CharField(max_length=30)
	jobRole = models.CharField(max_length=60)
	jobLocation = models.TextField()
	jobDescription = models.TextField()
	applyDeadline = models.DateField(help_text="Please use the following format: <em>YYYY-MM-DD</em>.")
	featured = models.BooleanField(default=False)
	approved=models.BooleanField(default=False)
	jobCreated = models.DateTimeField(auto_now_add=True, auto_now=False)
	jobEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['featured','-jobCreated','companyName']
		unique_together = ("companyName", "jobTitle")


class jobCategory(models.Model):
	categoryName = models.CharField(max_length=60,unique=True)
	dateCreated = models.DateTimeField(auto_now_add=True, auto_now=False)
	dateEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['categoryName']


class jobType(models.Model):
	typeName = models.CharField(max_length=60,unique=True)
	dateCreated = models.DateTimeField(auto_now_add=True, auto_now=False)
	dateEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['typeName']


class jobSalary(models.Model):
	salaryRange = models.CharField(max_length=30,unique=True)
	dateCreated = models.DateTimeField(auto_now_add=True, auto_now=False)
	dateEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['salaryRange']


class jobLocation(models.Model):
	locationName = models.CharField(max_length=100,unique=True)
	dateCreated = models.DateTimeField(auto_now_add=True)
	dateEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['locationName']


class jobRole(models.Model):
	roleName = models.CharField(max_length=60,unique=True)
	dateCreated = models.DateTimeField(auto_now_add=True)
	dateEdited = models.DateTimeField(auto_now_add=False, auto_now=True)

	class Meta:
		ordering = ['roleName']


class subscribe(models.Model):
	username = models.CharField(max_length=30)
	categoryName = models.CharField(max_length=60)

	class Meta:
		ordering = ['username','categoryName']
		unique_together = ("username", "categoryName")


class application(models.Model):
	name = models.CharField(max_length=120)
	username = models.CharField(max_length=30)
	job_id = models.IntegerField()
	created = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ['username','job_id']
		unique_together = ("username", "job_id")


class ads_left_block(models.Model):
	ads1=models.TextField()
	ads2=models.TextField()
	ads3=models.TextField()
	ads4=models.TextField()
	created=models.DateTimeField(auto_now_add=True,auto_now=False)
	edited=models.DateTimeField(auto_now_add=False,auto_now=True)
	class Meta:
		ordering=['ads1','ads2','ads3','ads4']

class ads_right_block(models.Model):
	ads1=models.TextField()
	ads2=models.TextField()
	ads3=models.TextField()
	ads4=models.TextField()
	created=models.DateTimeField(auto_now_add=True,auto_now=False)
	edited=models.DateTimeField(auto_now_add=False,auto_now=True)
	class Meta:
		ordering=['ads1','ads2','ads3','ads4']

class ads_bottom_block(models.Model):
	ads1=models.TextField()
	ads2=models.TextField()
	ads3=models.TextField()
	ads4=models.TextField()
	created=models.DateTimeField(auto_now_add=True,auto_now=False)
	edited=models.DateTimeField(auto_now_add=False,auto_now=True)
	class Meta:
		ordering=['ads1','ads2','ads3','ads4']


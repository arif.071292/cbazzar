import datetime
from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.template import RequestContext
from models import allJob
from models import jobCategory
from models import jobType
from models import jobSalary
from models import jobLocation
from models import jobRole
from models import application
from models import ads_left_block
from models import ads_right_block
from models import ads_bottom_block
from profiles.models import employerProfile
from profiles.models import userProfile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


today = datetime.date.today()


def go_to_Site(request):
	return HttpResponseRedirect('/careerbazzar/home')


def  go_to_Home(request):
	return HttpResponseRedirect('/careerbazzar/home/')


def  homePage(request):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	category = jobCategory.objects.values_list('categoryName',flat=True)
	jobtype = jobType.objects.values_list('typeName',flat=True)
	salary = jobSalary.objects.values_list('salaryRange',flat=True)
	location = jobLocation.objects.values_list('locationName',flat=True)
	featured = allJob.objects.filter(featured=True,applyDeadline__gte=today,approved=True)
	latest = allJob.objects.filter(applyDeadline__gte=today,approved=True)[0:10]
	ads_left=ads_left_block.objects.all()
	ads_right=ads_right_block.objects.all()
	ads_bottom=ads_bottom_block.objects.all()
	return render_to_response('site_home.html', locals(), context_instance=RequestContext(request))


def jobPost(request):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

		if request.method == "POST":
			cname = request.POST.get("cname","")
			cmail = request.POST.get("cmail","")
			title = request.POST.get("title","")
			category = request.POST.get("category","")
			jobtype = request.POST.get("type","")
			salary = request.POST.get("salary","")
			location = request.POST.get("location","")
			role = request.POST.get("role","")
			deadline = request.POST.get("date","")
			description = request.POST.get("description","")

			try:
				new_job = allJob(companyName=cname,companyEmail=cmail,jobTitle=title,jobCategory=category,jobType=jobtype,jobSalary=salary,jobRole=role,jobLocation=location,jobDescription=description,applyDeadline=deadline)
				new_job.save()
				return HttpResponseRedirect("/careerbazzar/home/")
			except:
				None

		try:
			profile = employerProfile.objects.get(cUserName=session_name)
		except:
			return HttpResponseRedirect('/careerbazzar/home/')

		category = jobCategory.objects.values_list('categoryName',flat=True)
		jobtype = jobType.objects.values_list('typeName',flat=True)
		salary = jobSalary.objects.values_list('salaryRange',flat=True)
		location = jobLocation.objects.values_list('locationName',flat=True)
		role = jobRole.objects.values_list('roleName',flat=True)
		
		return render_to_response('post_job.html', locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/careerbazzar/employer/signup/')


def jobPostEdit(request,job_id=1):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']
		try:
			old_job = allJob.objects.get(id=job_id)
			profile = employerProfile.objects.get(companyName=old_job.companyName)
			if session_name != profile.cUserName:
				return HttpResponseRedirect("/careerbazzar/home/")
		except:
			return HttpResponseRedirect("/careerbazzar/home/")
		

		if request.method == "POST":
			cname = request.POST.get("cname","")
			cmail = request.POST.get("cmail","")
			title = request.POST.get("title","")
			category = request.POST.get("category","")
			jobtype = request.POST.get("type","")
			salary = request.POST.get("salary","")
			location = request.POST.get("location","")
			role = request.POST.get("role","")

			if request.POST.get('date'):
				deadline = request.POST.get("date")

			description = request.POST.get("description","")

			addr = "/careerbazzar/job/"+job_id+"/"
			try:
				old_job.companyName = cname
				old_job.companyEmail = cmail
				old_job.jobTitle = title
				old_job.jobCategory = category
				old_job.jobType = jobtype
				old_job.jobSalary = salary
				old_job.jobRole = role
				old_job.jobLocation = location
				old_job.jobDescription = description

				if request.POST.get('date'):
					old_job.applyDeadline = deadline

				old_job.save()
				return HttpResponseRedirect(addr)
			except:
				return HttpResponseRedirect(addr)

		category = jobCategory.objects.exclude(categoryName=old_job.jobCategory)
		jobtype = jobType.objects.exclude(typeName=old_job.jobType)
		salary = jobSalary.objects.exclude(salaryRange=old_job.jobSalary)
		location = jobLocation.objects.exclude(locationName=old_job.jobLocation)
		role = jobRole.objects.exclude(roleName=old_job.jobRole)
		
		return render_to_response('post_job_edit.html', locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/careerbazzar/employer/signup/')


def browseJobs(request):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	category = jobCategory.objects.values_list('categoryName',flat=True)
	n = len(category)
	if n%3 == 0:
		grid1 = category[0:n/3]
		grid2 = category[n/3:(2*n)/3]
		grid3 = category[(2*n)/3:n]

	else:
		grid1 = category[0:(n/3)+1]
		grid2 = category[(n/3)+1:((2*n)/3)+1]
		grid3 = category[((2*n)/3)+1:n]


	return render_to_response('browseJobs.html', locals(), context_instance=RequestContext(request))


def viewJob(request, job_id=1):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

		app = application.objects.filter(username=session_name,job_id=job_id)
		if app:
			applied=True

	try:
		job = allJob.objects.get(id=job_id,applyDeadline__gte=today,approved=True)
		profile = employerProfile.objects.get(companyName=job.companyName)
	except:
		job = False

	try:
		profile2 = userProfile.objects.get(userName=session_name)
		user = True
	except:
		user = False
	
	category = jobCategory.objects.values_list('categoryName',flat=True)
	jobtype = jobType.objects.values_list('typeName',flat=True)
	salary = jobSalary.objects.values_list('salaryRange',flat=True)
	location = jobLocation.objects.values_list('locationName',flat=True)
		
	return render_to_response('jobView.html', locals(), context_instance=RequestContext(request))


def applyJob(request,job_id=1):
	addr = "/careerbazzar/job/" + job_id +"/"
	if 'name' in request.session:
		session_name = request.session['name']
		try:
			profile = userProfile.objects.get(userName=session_name)
			fname = profile.firstName + " " + profile.lastName
			apply2 =application(name=fname,username=session_name,job_id=job_id)
			apply2.save()
		except:
			return HttpResponseRedirect(addr)
		
		return HttpResponseRedirect(addr)

	return HttpResponseRedirect('/careerbazzar/login/')


def searchResult1(request):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	if request.method == "GET":
		keyword = request.GET.get("keyword","")
		jobtype = request.GET.get("type","")

		if jobtype=="All Types":
			result_list = allJob.objects.filter(jobTitle__icontains=keyword,applyDeadline__gte=today,approved=True)|allJob.objects.filter(companyName__icontains=keyword,applyDeadline__gte=today,approved=True)
			paginator=Paginator(result_list,5)
			page=request.GET.get('page')
			try:
				result=paginator.page(page)
			except PageNotAnInteger:
				result=paginator.page(1)
			except EmptyPage:
				result=paginator.page(paginator.num_pages)
		else:
			result_list = allJob.objects.filter(jobTitle__icontains=keyword,jobType=jobtype,applyDeadline__gte=today,approved=True)|allJob.objects.filter(companyName__icontains=keyword,jobType=jobtype,applyDeadline__gte=today,approved=True)
			paginator=Paginator(result_list,5)
			page=request.GET.get('page')
			try:
				result=paginator.page(page)
			except PageNotAnInteger:
				result=paginator.page(1)
			except EmptyPage:
				result=paginator.page(paginator.num_pages)

	return render_to_response('search_result2.html', locals(), context_instance=RequestContext(request))


def searchResult2(request):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	if request.method == "GET":
		category = request.GET.get("category","")
		jobtype = request.GET.get("type","")
		salary = request.GET.get("salary","")
		location = request.GET.get("location","")

		if category == "All":
			category = ""

		if jobtype == "All": 
			jobtype = ""

		if salary == "All":
			salary = ""

		if location == "All":
			location = ""

		result_list= allJob.objects.filter(jobCategory__icontains=category,jobType__icontains=jobtype,jobSalary__icontains=salary,jobLocation__icontains=location,applyDeadline__gte=today,approved=True)
		paginator=Paginator(result_list,5)
		page=request.GET.get('page')
		try:
			result=paginator.page(page)
		except PageNotAnInteger:
			result=paginator.page(1)
		except EmptyPage:
			result=paginator.page(paginator.num_pages)

	category = jobCategory.objects.values_list('categoryName',flat=True)
	jobtype = jobType.objects.values_list('typeName',flat=True)
	salary = jobSalary.objects.values_list('salaryRange',flat=True)
	location = jobLocation.objects.values_list('locationName',flat=True)  

	return render_to_response('search_result2.html', locals(), context_instance=RequestContext(request))


def filterCategory(request,category=""):
	if 'name' in request.session:
		login = True
		session_name = request.session['name']

	result_list = allJob.objects.filter(jobCategory=category,applyDeadline__gte=today,approved=True)
	paginator=Paginator(result_list,5)
	page=request.GET.get('page')
	try:
		result=paginator.page(page)
	except PageNotAnInteger:
		result=paginator.page(1)
	except EmptyPage:
		result=paginator.page(paginator.num_pages)

	return render_to_response('filter.html', locals(), context_instance=RequestContext(request))
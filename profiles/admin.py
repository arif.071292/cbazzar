from django.contrib import admin

from models import userProfile
from models import employerProfile
from models import siteAbout
from models import siteTerm
from models import privacyPolicy
from models import siteFAQ


class userProfileAdmin(admin.ModelAdmin):
	list_display = ('userName','email','eduInstitute')

admin.site.register(userProfile,userProfileAdmin)


class employerProfileAdmin(admin.ModelAdmin):
	list_display = ('companyName','companyEmail','companyCategory')

admin.site.register(employerProfile,employerProfileAdmin)


class siteAboutAdmin(admin.ModelAdmin):
	list_display = ('about',)

admin.site.register(siteAbout,siteAboutAdmin)


class siteTermAdmin(admin.ModelAdmin):
	list_display = ('termsofService',)

admin.site.register(siteTerm,siteTermAdmin)


class privacyPolicyAdmin(admin.ModelAdmin):
	list_display = ('privacyPolicy',)

admin.site.register(privacyPolicy,privacyPolicyAdmin)


class siteFAQAdmin(admin.ModelAdmin):
	list_display = ('id','question')

admin.site.register(siteFAQ,siteFAQAdmin)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_auto_20150303_0359'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='propic',
            field=models.FileField(null=True, upload_to=b'Photo/%Y/%m/%d', blank=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_employerprofile_description'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sitefaq',
            options={'ordering': ['id']},
        ),
        migrations.AlterField(
            model_name='sitefaq',
            name='question',
            field=models.TextField(unique=True),
            preserve_default=True,
        ),
    ]

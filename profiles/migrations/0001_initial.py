# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='employerProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('companyName', models.CharField(unique=True, max_length=160)),
                ('companyWebsite', models.CharField(max_length=160, null=True, blank=True)),
                ('companyUserName', models.CharField(unique=True, max_length=160)),
                ('companyEmail', models.EmailField(max_length=254)),
                ('companyLogo', models.FileField(null=True, upload_to=b'Files/%Y/%m/%d', blank=True)),
                ('companyAddress', models.TextField()),
                ('contactNo', models.CharField(max_length=15)),
                ('companyFax', models.CharField(max_length=20, null=True, blank=True)),
                ('companyCategory', models.CharField(max_length=60)),
                ('role', models.CharField(default=b'employer', max_length=10)),
                ('profileCreated', models.DateTimeField(auto_now_add=True)),
                ('profileModified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['companyName'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='privacyPolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('privacyPolicy', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('edited', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='siteAbout',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('about', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('edited', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='siteFAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('edited', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['question'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='siteTerm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('termsofService', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('edited', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='userProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstName', models.CharField(max_length=60)),
                ('lastName', models.CharField(max_length=60)),
                ('userName', models.CharField(unique=True, max_length=30)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('contactNo', models.CharField(max_length=15, null=True)),
                ('gender', models.CharField(max_length=10, null=True)),
                ('experienceLevel', models.CharField(max_length=60, null=True)),
                ('eduInstitute', models.CharField(max_length=120, null=True, blank=True)),
                ('degree', models.CharField(max_length=120, null=True, blank=True)),
                ('certification', models.TextField(null=True, blank=True)),
                ('keySkills', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('role', models.CharField(default=b'user', max_length=10)),
                ('profileCreated', models.DateTimeField(auto_now_add=True)),
                ('profileModified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['userName'],
            },
            bases=(models.Model,),
        ),
    ]

from django.contrib import admin
from models import loginTry
# Register your models here.

class loginTryAdmin(admin.ModelAdmin):
	list_display=('ip_address','date_tried')
admin.site.register(loginTry,loginTryAdmin)

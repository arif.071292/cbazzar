from django.db import models

# Create your models here.

class loginTry(models.Model):
	ip_address=models.CharField(max_length=30)
	date_tried=models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering=["ip_address","date_tried"]
